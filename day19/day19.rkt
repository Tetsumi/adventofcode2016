;;;
;;; Advent of Code 2016
;;;
;;; --- Day 19: An Elephant Named Joseph ---
;;;
;;; Tetsumi <tetsumi@vmail.me>
;;;

#lang racket

(define input (read))

(define (part1 n)
  (define sub1nlength (sub1 (integer-length n)))
  (define mask (arithmetic-shift 1 sub1nlength))
  (add1 (* 2 (bitwise-xor n mask))))

(define (part2 n)
  (do ([elve 1 (add1 elve)]
       [ma 1 (if (> (+ rem 2) elve) rem ma)]
       [rem 1 (cond [(> (+ rem 2) elve) 1]
                    [(< rem ma) (add1 rem)]
                    [else (+ rem 2)])])
      ((= elve (add1 input)) rem)))

(displayln (part1 input))
(displayln (part2 input))
  
