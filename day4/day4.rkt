;;;
;;; Advent of Code 2016
;;;
;;; --- Day 4: Security Through Obscurity ---
;;;
;;; Tetsumi <tetsumi@vmail.me>
;;;

#lang racket

(define (verify s)
  (define sp (string-split (string-replace s "-" "") #rx"\\[|\\]"))
  (define h (make-hash))

  (for ([c (in-string (car sp))]
        #:break (char-numeric? c))
    (hash-update! h c add1 0))
  
  (if (for/and ([c1 (in-string (cadr sp))]
                [c2 (in-list
                     (take (sort (hash->list h)
                                 (lambda (x y)
                                   (or (> (cdr x) (cdr y))
                                       (and (= (cdr x) (cdr y))
                                            (char<? (car x) (car y))))))
                           5))])
        (char=? c1 (car c2)))
      (let ([n (string->number (substring (car sp)
                                          (- (string-length (car sp)) 3)))])
        (for ([c (in-string s)]
              #:break (char-numeric? c))
          (display (if (char=? #\- c)
                       " " 
                       (integer->char (+ 97 (remainder (+ (- (char->integer c)
                                                             97)
                                                          n)
                                                       26))))))
        (displayln n)
        n)
      0))

(displayln (for/sum ([l (in-lines)]) (verify l)))
